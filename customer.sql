use sales;
CREATE TABLE IF NOT EXISTS customers( 
   customer_id INT AUTO_INCREMENT PRIMARY KEY,
   first_name VARCHAR(255) NOT NULL,
   last_name VARCHAR(255)NOT NULL,
   phone INT(40) NOT NULL,
   email VARCHAR(40) NOT NULL,
   street VARCHAR(50) NOT NULL, 
   city VARCHAR(50) NOT NULL,
   state VARCHAR(25) NOT NULL,
   zip_code INT(10) NOT NULL
   
);
CREATE TABLE IF NOT EXISTS orders( 
   order_id INT NOT NULL,
   customer_id INT NOT NULL,
   order_status VARCHAR(255) NOT NULL,
   order_date DATE(255)NOT NULL,
   required_date DATE(40) NOT NULL,
   shipped_date DATE(50) NOT NULL, 
   store_id INT(50) NOT NULL,
   staff_id INT(10) NOT NULL 
   FOREIGN KEY (customer_id)
   REFERENCES customers(customer_id)
);
CREATE TABLE IF NOT EXISTS staffs( 
   staff_id INT NOT NULL,
   first_name VARCHAR(255) NOT NULL,
   last_name VARCHAR(255)NOT NULL,
   email VARCHAR(40) NOT NULL,
   phone INT(40) NOT NULL,
   acive VARCHAR(50) NOT NULL, 
   store_id INT(50) NOT NULL,
   manager_id INT(10) NOT NULL 
   FOREIGN KEY (customer_id)
   REFERENCES customers(customer_id)
);
CREATE TABLE IF NOT EXISTS stores( 
   store_id INT NOT NULL,
   store_name VARCHAR(255) NOT NULL,
   phone INT(40) NOT NULL,
   email VARCHAR(40) NOT NULL,
   street VARCHAR(50) NOT NULL, 
   city VARCHAR(50) NOT NULL,
   state VARCHAR(25) NOT NULL,
   zip_code INT(10) NOT NULL
   FOREIGN KEY (customer_id)
   REFERENCES customers(customer_id)
);
